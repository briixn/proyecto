package com.proyecto.briix.poliarc


import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ReservacionService {

    @GET("/")
    fun getReservacion(
            @Query("offset") offset: Int = 0,
            @Query("limit") limit: Int = 20)
            : Call<Reservacion>

    @GET("/Reservaciones/{id}")
    fun getSingleReservacion(@Path("id") id: String)
            :Call<Reservacion>

    companion object {
        val instance: ReservacionService by lazy {
            val retrofit = Retrofit.Builder()
                    .baseUrl("https://poliarc-epn.firebaseio.com/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
            retrofit.create<ReservacionService>(ReservacionService::class.java)
        }
    }
}