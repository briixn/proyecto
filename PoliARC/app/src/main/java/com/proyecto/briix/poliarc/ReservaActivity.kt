package com.proyecto.briix.poliarc

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class ReservaActivity : AppCompatActivity(), ReservaAdapter.ReservaSelectionListener{

  companion object {
    val INTENT_POKEMON_NAME = "reservaName"
  }

  lateinit var reservaRecyclerView: androidx.recyclerview.widget.RecyclerView

  lateinit var linearLayoutManager: androidx.recyclerview.widget.LinearLayoutManager
  lateinit var scrollListener: androidx.recyclerview.widget.RecyclerView.OnScrollListener
  private val lastVisiblePosition:Int
    get() = linearLayoutManager.findLastVisibleItemPosition()


  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_pokedex)

    linearLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)

    reservaRecyclerView = findViewById(R.id.pokedex_recycler_view)
    reservaRecyclerView.layoutManager = linearLayoutManager
    reservaRecyclerView.adapter = ReservaAdapter(this)

  }

  fun showSinglePokemon(pokemonName:String) {
    val singlePokemonIntent = Intent(this, SingleReservacionActivity::class.java)
    singlePokemonIntent.putExtra(INTENT_POKEMON_NAME, pokemonName)
    startActivity(singlePokemonIntent)
  }

  override fun reservacionSelected(pokemonName: String) {
    showSinglePokemon(pokemonName)
  }

  private fun setViewScrollListener() {
    scrollListener = object : androidx.recyclerview.widget.RecyclerView.OnScrollListener() {
      override fun onScrollStateChanged(recyclerView: androidx.recyclerview.widget.RecyclerView, newState: Int) {
        super.onScrollStateChanged(recyclerView, newState)
        val totalItemCount = recyclerView.layoutManager!!.itemCount
        if (totalItemCount == lastVisiblePosition + 1) {

          val adapter = reservaRecyclerView.adapter as ReservaAdapter
          adapter.loadReservacion()
          reservaRecyclerView.removeOnScrollListener(scrollListener)
        }
      }
    }

    reservaRecyclerView.addOnScrollListener(scrollListener)
  }

  override fun fetchCompleted() {
    setViewScrollListener()
  }
}
