package com.proyecto.briix.poliarc

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.pokedex_view_holder.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ReservaAdapter(val selectionListener: ReservaSelectionListener): androidx.recyclerview.widget.RecyclerView.Adapter<ReservaAdapter.ReservaViewHolder>() {

    companion object {
        val LIMIT = 20
    }

    interface ReservaSelectionListener {
        fun reservacionSelected(pokemonName: String)///FIJAR PARAMETRO A ESCOGER posiblemente fecha+id
        fun fetchCompleted()
    }

    var reservacionList = mutableListOf<Reservacion>()
    var currentOffset = 0

    init {
        loadReservacion()
    }

    fun loadReservacion() {

        val reservacionService = ReservacionService.instance
        val reservacionCall = reservacionService.getReservacion(currentOffset, LIMIT)

        reservacionCall.enqueue(object : Callback<Reservacion> {
            override fun onFailure(call: Call<Reservacion>?, t: Throwable?) {
                if (call != null) {
                }
            }

            override fun onResponse(call: Call<Reservacion>?, response: Response<Reservacion>?) {

                if (response != null) {
                    val body = response.body() as Reservacion
                    reservacionList.addAll(body.cancha as MutableList<Reservacion>)
                    notifyDataSetChanged()
                    currentOffset += LIMIT
                    selectionListener.fetchCompleted()
                }
            }
        })

    }
////////////////EDITAAAAAAAAAAAAR AQUIIIIIIIIIIIIIIIIIIII

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ReservaViewHolder {
        val view = LayoutInflater.from(p0.context)
                .inflate(R.layout.pokedex_view_holder, p0, false)
        return ReservaViewHolder(view)
    }

    override fun getItemCount(): Int {
        return reservacionList.count()
    }

    override fun onBindViewHolder(p0: ReservaViewHolder, p1: Int) {

        p0.reservacionName.text = reservacionList[p1].equipo1 + " vs " +reservacionList[p1].equipo2
        p0.fecha.text = reservacionList[p1].dia
        p0.hora.text = reservacionList[p1].hora


        /* Glide.with(p0.itemView)
                 .load("https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${p1 + 1}.png")
                 .into(p0.pokemonImage)
 */
        p0.itemView.setOnClickListener {
            selectionListener.reservacionSelected(reservacionList[p1].equipo1+reservacionList[p1].equipo2)
        }
    }


    class ReservaViewHolder (itemView:View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {

        val reservacionName = itemView.equipos
        val fecha = itemView.fecha
        val hora = itemView.hora

        //val pokemonImage = itemView.findViewById<ImageView>(R.id.pokemon_image)
    }
}
