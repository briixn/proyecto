package com.proyecto.briix.poliarc

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.registrofacultad.*
import kotlinx.android.synthetic.main.reservacanchas.*

class ReservaCanchas : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.reservacanchas)
        botonBasket.setOnClickListener {cargarIntent(botonBasket.text.toString()) }
        botonFutbol.setOnClickListener { cargarIntent(botonFutbol.text.toString()) }
        botonVoley.setOnClickListener {cargarIntent(botonVoley.text.toString()) }
        botonRegresar6.setOnClickListener { cargarMain() }



    }

    fun cargarMain(){
        val intent = Intent(this,MainActivity::class.java)
        startActivity(intent)
    }
    fun cargarIntent(deporte:String){
        val intent = Intent(this,Cancha::class.java)
        intent.putExtra("deporte",deporte)
        startActivity(intent)
    }




}
