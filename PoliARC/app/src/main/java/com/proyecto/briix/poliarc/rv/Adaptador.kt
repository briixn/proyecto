package com.proyecto.briix.poliarc.rv

import android.content.Intent
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.proyecto.briix.poliarc.MainActivity
import com.proyecto.briix.poliarc.R.layout.row_recycler

import com.proyecto.briix.poliarc.Reservacion
import kotlinx.android.synthetic.main.row_recycler.view.*

class Adaptador : androidx.recyclerview.widget.RecyclerView.Adapter<Adaptador.ReservasViewHolder> {


    internal lateinit var reservas: List<Reservacion>

    constructor() {}
    constructor(reservas: List<Reservacion>) {
        this.reservas = reservas
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReservasViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(row_recycler, parent, false)
        return ReservasViewHolder( v)
    }

    override//se hara todolo que se hara en el recycler como rellenar loscampos
    fun onBindViewHolder(holder: ReservasViewHolder, position: Int) {
        val p = reservas[position]
        holder.equipo1p.setText(p.equipo1)
        holder.equipo2p.setText(p.equipo2)
        holder.cancha.setText(p.cancha)
        holder.hora.setText(p.hora)
        holder.dia.setText(p.dia)
        holder.id.setText(p.id)
    }

    override fun getItemCount(): Int {
        return reservas.size
    }

    class ReservasViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {
        internal var equipo1p: TextView
        internal var equipo2p: TextView
        internal var dia: TextView
        internal var hora: TextView
        internal var id: TextView
        internal var cancha: TextView

        init {
            equipo1p = itemView.txtEquipo1

            equipo2p = itemView.txtEquipo2

            dia = itemView.txtDia

            hora = itemView.txtHora

            id = itemView.txtId

            cancha = itemView.txtCancha


        }

        override fun hashCode(): Int {
            return super.hashCode()
        }

        override fun equals(obj: Any?): Boolean {
            return super.equals(obj)
        }



        override fun toString(): String {
            return super.toString()
        }


    }

}

fun cargarMain(){

}
