package com.proyecto.briix.poliarc

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SingleReservacionActivity : AppCompatActivity() {

  lateinit var pokemonName: String
  lateinit var pokemonHeight: TextView
  lateinit var pokemonWeight: TextView

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_single_reservacion)
    pokemonHeight = findViewById(R.id.pokemon_height)
    pokemonWeight = findViewById(R.id.pokemon_weight)

    pokemonName = intent.getStringExtra(ReservaActivity.INTENT_POKEMON_NAME)
    title = pokemonName

    val pokemonService = ReservacionService.instance
    val singlePokemonCall = pokemonService.getSingleReservacion(pokemonName)

    singlePokemonCall.enqueue(object : Callback<Reservacion
            > {
      override fun onFailure(call: Call<Reservacion
              >?, t: Throwable?) {
        if (call != null) {
        }
      }

      override fun onResponse(call: Call<Reservacion
              >?, response: Response<Reservacion
              >?) {
        if (response != null) {
          val body = response.body() as Reservacion

          pokemonHeight.text = getString(R.string.horario, body.hora)
          pokemonWeight.text = getString(R.string.fecha, body.dia)
        }
      }
    })

  }
}
