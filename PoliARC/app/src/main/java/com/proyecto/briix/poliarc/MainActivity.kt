package com.proyecto.briix.poliarc

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.DatabaseReference;

import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        botonReservaCanchas.setOnClickListener {cargarRC() }
        botonInterfacultad.setOnClickListener { cargarInterfacultades() }
    }
    fun cargarRC(){
        val intent = Intent(this,ReservaCanchas::class.java)
        startActivity(intent)
    }
    fun cargarInterfacultades(){
        val intent = Intent(this,Interfacultades::class.java)
        startActivity(intent)
    }
}
