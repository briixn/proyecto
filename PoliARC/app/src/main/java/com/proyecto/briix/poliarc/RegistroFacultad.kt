package com.proyecto.briix.poliarc

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.registrofacultad.*

class RegistroFacultad: AppCompatActivity() {
    var equipo1 = ""
    var equipo2 = ""
    var it =0

    private var mDatabase: DatabaseReference? = null



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.registrofacultad)

        // ...
        mDatabase = FirebaseDatabase.getInstance().getReference();

        crearSpinner()
        botonGuardar.setOnClickListener{
            accionGuardar()

        }

        botonCancelar.setOnClickListener{
            accionCancelar()

        }


        botonRegresar4.setOnClickListener { cargarMain() }



    }

    fun cargarMain(){
        val intent = Intent(this,MainActivity::class.java)
        startActivity(intent)
    }

    fun accionGuardar(){
        var id = mDatabase?.push()?.key.toString()
        mDatabase?.child("Reservaciones")?.child(id)?.setValue(crearReserva(id))

        Toast.makeText(this,"Cancha reservada exitosamente!!",Toast.LENGTH_LONG)
        val intent = Intent(this,MainActivity::class.java)
        startActivity(intent)
    }

    fun accionCancelar(){
        Toast.makeText(this,"Escoja el horario preferido",Toast.LENGTH_LONG)
        val intent = Intent(this,Reserva::class.java)
        startActivity(intent)
    }

    fun crearReserva(id:String):Reservacion{
        var dia = intent.extras.get("dia").toString()
        var hora = intent.extras.get("time").toString()
        var cancha = intent.extras.get("canch").toString()
        var deporte = intent.extras.get("deport").toString()

        var reserva =  Reservacion(id,dia,hora,equipo1,equipo2,cancha,deporte)
        return reserva
    }




    fun crearSpinner(){
        val adapter = ArrayAdapter(
                this, // Context
                android.R.layout.simple_spinner_item, // Layout
                crearLista() // ListaFacultades
        )
        // Set the drop down view resource
        adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line)

        // Finally, data bind the spinner object with dapter
        spinnerEquipo1.adapter = adapter;
        spinnerEuipo2.adapter= adapter;

        // Set an on item selected listener for spinner object
        spinnerEquipo1.onItemSelectedListener = object: AdapterView.OnItemSelectedListener{
            override fun onItemSelected(parent:AdapterView<*>, view: View, position: Int, id: Long){
                // Display the selected item text on text view
                equipo1= parent.getItemAtPosition(position).toString()
            }

            override fun onNothingSelected(parent: AdapterView<*>){
                // Another interface callback
            }
        }

        //Spinner2
        spinnerEuipo2.onItemSelectedListener = object: AdapterView.OnItemSelectedListener{
            override fun onItemSelected(parent:AdapterView<*>, view: View, position: Int, id: Long){
                // Display the selected item text on text view
                equipo2= parent.getItemAtPosition(position).toString()
            }

            override fun onNothingSelected(parent: AdapterView<*>){
                // Another interface callback
            }
        }



    }

    fun crearLista():List<String>{
        val facultades = listOf("Física",
        "Ingeniería Matemática" ,
                "Ingeniería en Ciencias Económicas y Financieras",
                "Ingeniería Empresarial",
                "Ingeniería Civil" ,
                "Ingeniería Ambiental",
                "Ingeniería Eléctrica" ,
                "Ingeniería Electrónica y Control" ,
                "Ingeniería Electrónica y Redes de Información" ,
                "Ingeniería en Electrónica y Telecomunicaciones",
                "Ingeniería en Geología" ,
                "Ingeniería en Petróleos",
                "Ingeniería Mecánica",
                "Ingeniería Química" ,
                "Ingeniería Agroindustrial",
                "Ingeniería en Sistemas Informáticos y de Computación",
                "Tecnología en Electrónica y Telecomunicaciones" ,
                "Tecnología en Análisis de Sistemas Informáticos",
                "Tecnología en Electromecánica",
                "Tecnología en Agua y Saneamiento Ambiental")
        return facultades
    }
}

