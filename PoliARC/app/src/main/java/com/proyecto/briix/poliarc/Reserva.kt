package com.proyecto.briix.poliarc

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.registrofacultad.*
import kotlinx.android.synthetic.main.reserva.*

class Reserva: AppCompatActivity(),AdapterView.OnItemSelectedListener {
    var dia =""
    var horarios= arrayOf("7:00 - 8:00",
    "8:00 - 9:00",
    "9:00 - 10:00",
    "10:00 - 11:00",
    "11:00 - 12:00",
    "12:00 - 13:00",
    "13:00 - 14:00",
    "14:00 - 15:00",
    "15:00 - 16:00",
    "16:00 - 17:00",
    "17:00 - 18:00",
    "18:00 - 19:00")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.reserva)

        var hora = ""


        var spinner:Spinner? = null

        spinner = this.spinnerHorarios
        spinner!!.setOnItemSelectedListener(this)

        // Create an ArrayAdapter using a simple spinner layout and languages array
        val aa = ArrayAdapter(this, android.R.layout.simple_spinner_item, horarios)
        // Set layout to use when the list of choices appear
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        // Set Adapter to Spinner
        spinner!!.setAdapter(aa)



        botonRegresar5.setOnClickListener { cargarMain() }


    }

    override fun onItemSelected(arg0: AdapterView<*>, arg1: View, position: Int, id: Long) {
        var time = horarios[position]
        //hora=  parent.getItemAtPosition(position).toString()
        botonReservar.setOnClickListener { reservar(time)}

    }

    override fun onNothingSelected(arg0: AdapterView<*>) {

    }

    fun cargarMain(){
        val intent = Intent(this,MainActivity::class.java)
        startActivity(intent)
    }

    fun reservar(horario: String){


        val cancha = intent.extras.get("cancha").toString()
        val deporte = intent.extras.get("deporte").toString()
        val intent = Intent(this,RegistroFacultad::class.java)


      var calendario = calendarView.setOnDateChangeListener { view, year, month, dayOfMonth ->


          dia = ("" + dayOfMonth + "/" + (month + 1) + "/" + year)
          // intent.putExtra("dia", dia)

      }
          intent.putExtra("time", horario)
          intent.putExtra("dia", calendario.toString())
          intent.putExtra("canch", cancha)
          intent.putExtra("deport", deporte)

          startActivity(intent)
      }
    }

//mirar kotlin formato unix timestampt y luego setear
