package com.proyecto.briix.poliarc.rv

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.proyecto.briix.poliarc.MainActivity
import com.proyecto.briix.poliarc.R
import com.proyecto.briix.poliarc.Reservacion
import kotlinx.android.synthetic.main.activity_tienda.*
import kotlinx.android.synthetic.main.cancha.*
import kotlinx.android.synthetic.main.row_recycler.*

import java.util.ArrayList

class Tienda : AppCompatActivity() {
    internal lateinit var rv: androidx.recyclerview.widget.RecyclerView
    internal  lateinit var reservas: MutableList<Reservacion>
    internal lateinit var adaptador: Adaptador
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tienda)

        rv = recycler

        rv.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        reservas = ArrayList<Reservacion>()

        val database = FirebaseDatabase.getInstance().getReference("Reservaciones")

        adaptador = Adaptador(reservas)
        rv.adapter = adaptador

        database.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                reservas.removeAll(reservas)
                for (snapshot in dataSnapshot.children) {
                    var pro = snapshot.getValue(Reservacion::class.java)
                    reservas.add(pro!!)
                }
                adaptador.notifyDataSetChanged()
                //Cada que se cambien datos los añadira y nunca borrara :O


            }

            override fun onCancelled(databaseError: DatabaseError) {

            }
        })

    }

}
