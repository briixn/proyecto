package com.proyecto.briix.poliarc

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.cancha.*
import kotlinx.android.synthetic.main.registrofacultad.*

class Cancha: AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.cancha)
        cargarImagen()

        botonCancha1.setOnClickListener { cargarIntent(botonCancha1.text.toString()) }
        botonCancha2.setOnClickListener { cargarIntent(botonCancha2.text.toString()) }
        botonCancha3.setOnClickListener { cargarIntent(botonCancha3.text.toString()) }
        botonRegresar.setOnClickListener { cargarMain() }



    }

    fun cargarMain(){
        val intent = Intent(this,MainActivity::class.java)
        startActivity(intent)
    }

    fun cargarIntent(cancha: String ){
        var tipoDeporte:String = intent.extras.get("deporte").toString()

        val intent = Intent(this,Reserva::class.java)
        intent.putExtra("cancha",cancha)
        intent.putExtra("deporte",tipoDeporte)

        startActivity(intent)
    }



    fun cargarImagen(){
        var tipoDeporte:String = intent.extras.get("deporte").toString()

        if(tipoDeporte.equals("FUTBOL")){
        imageViewDeporte.setImageResource(R.drawable.futbol)
        textViewDeporteEscogido.text = getString(R.string.futbol);
    }
        if(tipoDeporte.equals("VOLEIBOL")){
            imageViewDeporte.setImageResource(R.drawable.voley)
            textViewDeporteEscogido.text = getString(R.string.voleybol);

        }else{
            imageViewDeporte.setImageResource(R.drawable.basket)
            textViewDeporteEscogido.text = getString(R.string.basquetbol);


        }

    }

}