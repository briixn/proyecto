package com.proyecto.briix.poliarc

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.proyecto.briix.poliarc.rv.Tienda
import kotlinx.android.synthetic.main.interfacultades.*
import kotlinx.android.synthetic.main.registrofacultad.*

class Interfacultades: AppCompatActivity() {
    private var bdd: DatabaseReference? = null

    // ...
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.interfacultades)
        bdd = FirebaseDatabase.getInstance().getReference();

        buttonCronograma.setOnClickListener { cargarPartidos() }

        botonRegresar3.setOnClickListener { cargarMain() }



    }

    fun cargarMain(){
        val intent = Intent(this,MainActivity::class.java)
        startActivity(intent)
    }
    fun cargarPartidos(){
        val intent = Intent(this,Tienda::class.java)
        startActivity(intent)
    }

    }

